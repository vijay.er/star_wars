import graphene
from graphene_django.debug import DjangoDebug
import warsgraphql.schema


class Query(
    warsgraphql.schema.Query,
    graphene.ObjectType,

):
    debug = graphene.Field(DjangoDebug, name="_debug")


class Mutation(
    warsgraphql.schema.Mutation,
    graphene.ObjectType,
):
    debug = graphene.Field(DjangoDebug, name="_debug")


schema = graphene.Schema(query=Query, mutation=Mutation)
