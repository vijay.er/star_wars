from django.contrib import admin
from django.urls import path, include
from wars.views import *

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('wars.urls')),
    path('', include('warsgraphql.urls')),

]
