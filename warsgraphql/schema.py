import graphene
from graphene_django.filter import DjangoFilterConnectionField
from warsgraphql.validators import *
from wars.models import *
from graphene_django.types import DjangoObjectType
from graphene import Node
import requests


class PlanetType(DjangoObjectType):
    class Meta:
        model = Planet
        interfaces = (Node,)
        filter_fields = '__all__'


class PeopleType(DjangoObjectType):
    class Meta:
        model = People
        interfaces = (Node,)
        filter_fields = '__all__'


class StarshipType(DjangoObjectType):
    class Meta:
        model = Starship
        interfaces = (Node,)
        filter_fields = '__all__'


class VehicleType(DjangoObjectType):
    class Meta:
        model = Vehicle
        interfaces = (Node,)
        filter_fields = '__all__'


class SpeciesType(DjangoObjectType):
    class Meta:
        model = Species
        interfaces = (Node,)
        filter_fields = '__all__'


class FilmType(DjangoObjectType):
    class Meta:
        model = Film
        interfaces = (Node,)
        filter_fields = '__all__'


class Query(graphene.ObjectType):
    planet = graphene.Field(PlanetType)
    all_planet = DjangoFilterConnectionField(PlanetType)

    people = graphene.Field(PeopleType)
    all_people = DjangoFilterConnectionField(PeopleType)

    starship = graphene.List(StarshipType)
    all_starship = DjangoFilterConnectionField(StarshipType)

    vehicle = graphene.List(VehicleType)
    all_vehicle = DjangoFilterConnectionField(VehicleType)

    species = graphene.List(SpeciesType)
    all_species = DjangoFilterConnectionField(SpeciesType)

    films = graphene.List(FilmType)
    all_films = DjangoFilterConnectionField(FilmType)


class CreatePlanet(graphene.Mutation):
    id = graphene.Int()
    name = graphene.String()
    rotation_period = graphene.String()
    orbital_period = graphene.String()
    diameter = graphene.String()
    climate = graphene.String()
    gravity = graphene.String()
    terrain = graphene.String()
    surface_water = graphene.String()
    population = graphene.String()

    class Arguments:
        name = graphene.String()
        rotation_period = graphene.String()
        orbital_period = graphene.String()
        diameter = graphene.String()
        climate = graphene.String()
        gravity = graphene.String()
        terrain = graphene.String()
        surface_water = graphene.String()
        population = graphene.String()

    def mutate(self, info, name, rotation_period, orbital_period, climate, gravity, terrain, surface_water,
               population):
        if not validate_name(name):
            raise GraphQLError
        create_planet = Planet(name=name,
                               rotation_period=rotation_period,
                               orbital_period=orbital_period,
                               climate=climate,
                               gravity=gravity,
                               terrain=terrain,
                               surface_water=surface_water,
                               population=population)
        create_planet.save()
        return CreatePlanet(
            id=create_planet.id,
            name=create_planet.name,

        )


class CreatePeople(graphene.Mutation):
    id = graphene.Int()
    name = graphene.String()
    height = graphene.String()
    mass = graphene.String()
    hair_color = graphene.String()
    skin_color = graphene.String()
    eye_color = graphene.String()
    birth_year = graphene.String()
    gender = graphene.String()

    class Arguments:
        name = graphene.String()
        height = graphene.String()
        mass = graphene.String()
        hair_color = graphene.String()
        skin_color = graphene.String()
        eye_color = graphene.String()
        birth_year = graphene.String()
        gender = graphene.String()

    def mutate(self, info, name, height, mass, hair_color, skin_color, eye_color, birth_year,
               gender):

        if not validate_name(name):
            raise GraphQLError
        if not validate_height(height):
            raise GraphQLError
        if not validate_birth_year(birth_year):
            raise GraphQLError
        if not validate_gender(gender):
            raise GraphQLError

        create_people = People(name=name,
                               height=height,
                               mass=mass,
                               hair_color=hair_color,
                               skin_color=skin_color,
                               eye_color=eye_color,
                               birth_year=birth_year,
                               gender=gender)

        create_people.save()
        return CreatePeople(
            id=create_people.id,
            name=create_people.name,

        )


class CreateStarship(graphene.Mutation):
    id = graphene.Int()
    name = graphene.String()
    model = graphene.String()
    manufacturer = graphene.String()
    cost_in_credits = graphene.String()
    length = graphene.String()
    max_atmosphering_speed = graphene.String()
    crew = graphene.String()
    passengers = graphene.String()
    cargo_capacity = graphene.String()
    consumables = graphene.String()
    hyperdrive_rating = graphene.String()
    MGLT = graphene.String()
    starship_class = graphene.String()

    class Arguments:
        name = graphene.String()
        model = graphene.String()
        manufacturer = graphene.String()
        cost_in_credits = graphene.String()
        length = graphene.String()
        max_atmosphering_speed = graphene.String()
        crew = graphene.String()
        passengers = graphene.String()
        cargo_capacity = graphene.String()
        consumables = graphene.String()
        hyperdrive_rating = graphene.String()
        MGLT = graphene.String()
        starship_class = graphene.String()

    def mutate(self, info, name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew,
               passengers, cargo_capacity, consumables, hyperdrive_rating, MGLT, starship_class):
        create_starship = Starship(name=name,
                                   model=model,
                                   manufacturer=manufacturer,
                                   cost_in_credits=cost_in_credits,
                                   length=length,
                                   max_atmosphering_speed=max_atmosphering_speed,
                                   crew=crew,
                                   passengers=passengers,
                                   cargo_capacity=cargo_capacity,
                                   consumables=consumables,
                                   hyperdrive_rating=hyperdrive_rating,
                                   MGLT=MGLT,
                                   starship_class=starship_class)
        create_starship.save()
        return CreateStarship(
            id=create_starship.id,
            name=create_starship.name
        )


class CreateVehicle(graphene.Mutation):
    id = graphene.Int()
    name = graphene.String()
    model = graphene.String()
    manufacturer = graphene.String()
    cost_in_credits = graphene.String()
    length = graphene.String()
    max_atmosphering_speed = graphene.String()
    crew = graphene.String()
    passengers = graphene.String()
    cargo_capacity = graphene.String()
    consumables = graphene.String()
    vehicle_class = graphene.String()

    class Arguments:
        name = graphene.String()
        model = graphene.String()
        manufacturer = graphene.String()
        cost_in_credits = graphene.String()
        length = graphene.String()
        max_atmosphering_speed = graphene.String()
        crew = graphene.String()
        passengers = graphene.String()
        cargo_capacity = graphene.String()
        consumables = graphene.String()
        vehicle_class = graphene.String()

    def mutate(self, info, name, model, manufacturer, cost_in_credits, length, max_atmosphering_speed, crew,
               passengers, cargo_capacity, consumables, vehicle_class):
        create_vehicle = Vehicle(name=name,
                                 model=model,
                                 manufacturer=manufacturer,
                                 cost_in_credits=cost_in_credits,
                                 length=length,
                                 max_atmosphering_speed=max_atmosphering_speed,
                                 crew=crew,
                                 passengers=passengers,
                                 cargo_capacity=cargo_capacity,
                                 consumables=consumables,
                                 vehicle_class=vehicle_class,
                                 )
        create_vehicle.save()
        return CreateVehicle(
            id=create_vehicle.id,
            name=create_vehicle.name,
        )


class CreateSpecies(graphene.Mutation):
    id = graphene.Int()
    name = graphene.String()
    classification = graphene.String()
    designation = graphene.String()
    average_height = graphene.String()
    skin_colors = graphene.String()
    hair_colors = graphene.String()
    eye_colors = graphene.String()
    average_lifespan = graphene.String()
    language = graphene.String()

    class Arguments:
        name = graphene.String()
        classification = graphene.String()
        designation = graphene.String()
        average_height = graphene.String()
        skin_colors = graphene.String()
        hair_colors = graphene.String()
        eye_colors = graphene.String()
        average_lifespan = graphene.String()
        language = graphene.String()

    def mutate(self, info, name, classification, designation, average_height, skin_colors, hair_colors, eye_colors,
               average_lifespan, language):
        create_species = Species(name=name,
                                 classification=classification,
                                 designation=designation,
                                 average_height=average_height,
                                 skin_colors=skin_colors,
                                 hair_colors=hair_colors,
                                 eye_colors=eye_colors,
                                 average_lifespan=average_lifespan,
                                 language=language)
        create_species.save()
        return CreateSpecies(
            id=create_species.id,
            name=create_species.name,

        )


class CreateFilms(graphene.Mutation):
    id = graphene.Int()
    title = graphene.String()
    episode_id = graphene.String()
    opening_crawl = graphene.String()
    director = graphene.String()
    producer = graphene.String()
    release_date = graphene.String()

    class Arguments:
        title = graphene.String()
        episode_id = graphene.String()
        opening_crawl = graphene.String()
        director = graphene.String()
        producer = graphene.String()
        release_date = graphene.String()

    def mutate(self, info, title, episode_id, opening_crawl, director, producer, release_date):
        create_films = Film(title=title,
                            episode_id=episode_id,
                            opening_crawl=opening_crawl,
                            director=director,
                            producer=producer,
                            release_date=release_date)
        create_films.save()
        return CreateFilms(
            id=create_films.id,
            title=create_films.title,

        )


class Weather_api(graphene.Mutation):
    city = graphene.String()

    class Arguments:
        city = graphene.String()

    def mutate(self, info, city):
        url = "http://api.openweathermap.org/data/2.5/weather?appid=dbd96c1292f8917062920582e5901142&q="
        url = url + format(city)
        json_data = requests.get(url).json()
        print(json_data)
        return Weather_api(json_data)
        # {'city': city},
        # {'desctiption': json_data['weather'][0]['description']}})
        # 'temperature': json_data[0]['temperature'],
        # 'country': json_data[0]['country']})


class Mutation(object):
    create_planet = CreatePlanet.Field()
    create_people = CreatePeople.Field()
    create_species = CreateSpecies.Field()
    create_vehicle = CreateVehicle.Field()
    create_starship = CreateStarship.Field()
    create_films = CreateFilms.Field()
    check_weather = Weather_api.Field()

    # planet
    # (
    # name: "earth",
    # rotationPeriod: "23",
    # orbitalPeriod: "378",
    # climate: "arid, temperate, tropical",
    # gravity: "1",
    # terrain: "rainforests, cliffs, canyons, seas",
    # surfaceWater: "unknown",
    # population: "4000000000"
    # )

    # people(
    #   name: "asddasdfsd",
    #   height: "105",
    #   mass: "77",
    #   hairColor: "auburn, white",
    #   skinColor: "fair",
    #   eyeColor: "blue-gray",
    #   birthYear: "1194",
    #   gender: "female"
    # )

    # films(
    #     title: "wonder-woman",
    #     episodeId: "4",
    #     openingCrawl: "It is a period of civil war.\r\nRebel spaceships, striking\r\nfrom a hidden base, have won\r\ntheir first victory against\r\nthe evil Galactic Empire.\r\n\r\nDuring the battle, Rebel\r\nspies managed to steal secret\r\nplans to the Empire's\r\nultimate weapon, the DEATH\r\nSTAR, an armored space\r\nstation with enough power\r\nto destroy an entire planet.\r\n\r\nPursued by the Empire's\r\nsinister agents, Princess\r\nLeia races home aboard her\r\nstarship, custodian of the\r\nstolen plans that can save her\r\npeople and restore\r\nfreedom to the galaxy....",
    #     director: "George Lucas",
    #     producer: "Gary Kurtz, Rick McCallum",
    #     releaseDate: "1977-05-25"
    # )

    # starship (
    #     name: "star-wars",
    #     model: "Alpha-3 Nimbus-class V-wing starfighter",
    #     manufacturer: "Kuat Systems Engineering",
    #     costInCredits: "102500",
    #     length: "7.9",
    #     maxAtmospheringSpeed: "1050",
    #     crew: "1",
    #     passengers: "0",
    #     cargoCapacity: "60",
    #     consumables: "15 hours",
    #     hyperdriveRating: "1.0",
    #     MGLT: "unknown",
    #     starshipClass: "starfighter"
    # )

    # vehicle(
    #     name: "R1-car",
    #     model: "All Terrain Recon Transport",
    #     manufacturer: "Kuat Drive Yards",
    #     costInCredits: "40000",
    #     length: "3.2",
    #     maxAtmospheringSpeed: "90",
    #     crew: "1",
    #     passengers: "0",
    #     cargoCapacity: "20",
    #     consumables: "1 day",
    #     vehicleClass: "walker"
    # )

    # species(
    #     name: "Tritors",
    #     classification: "mammal",
    #     designation: "sentient",
    #     averageHeight: "190",
    #     skinColors: "grey",
    #     hairColors: "none",
    #     eyeColors: "black",
    #     averageLifespan: "700",
    #     language: "Utapese"
    #
    # )
