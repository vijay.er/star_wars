from django.apps import AppConfig


class WarsgraphqlConfig(AppConfig):
    name = 'warsgraphql'
