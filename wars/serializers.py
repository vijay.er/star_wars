from abc import ABC

from rest_framework import serializers
from .models import *


class TrackListingField(serializers.RelatedField):
    def to_representation(self, value):
        return {'id': value.id, 'name': value.name}


class PlanetSerializer(serializers.ModelSerializer):
    class Meta:
        model = Planet
        # fields = '__all__'
        exclude = ('created', 'edited')


class PeopleSerializer(serializers.ModelSerializer):
    homeworld = TrackListingField(read_only=True)

    class Meta:
        model = People
        # fields = '__all__'
        exclude = ('created', 'edited')

    @staticmethod
    def validate_name(value):
        """
        name validation
        """
        if len(value) < 5:
            raise serializers.ValidationError({'name': 'name to short'})

        for i in range(len(list(value))):
            if value[i].isdigit():
                raise serializers.ValidationError({'name': 'use alphabatical letters only'})

        return value

    @staticmethod
    def validate_height(value):
        """
        height validation
        """
        if len(str(value)) != 3:
            raise serializers.ValidationError({'height': 'enter proper value'})

        if not value.isdigit():
            raise serializers.ValidationError({'height': 'use digits only'})

        return value

    @staticmethod
    def validate_birth_year(value):
        """
        validate birth year
        """
        if not value.isdigit():
            raise serializers.ValidationError({'birth_year': 'use digits only'})

        if len(str(value)) != 4:
            raise serializers.ValidationError({'birth_year': 'enter proper value'})

        return value

    @staticmethod
    def validate_gender(value):
        """
        validate gender
        """
        if value != 'male' and value != 'female':
            raise serializers.ValidationError({'gender': 'enter proper value'})

        return value


class StarshipSerializer(serializers.ModelSerializer):
    pilots = TrackListingField(many=True, read_only=True)

    class Meta:
        model = Starship
        exclude = ('created', 'edited')
        # depth = 1


class VehicleSerializer(serializers.ModelSerializer):
    pilots = TrackListingField(many=True, read_only=True)

    class Meta:
        model = Vehicle
        exclude = ('created', 'edited')
        # depth = 1


class SpeciesSerializer(serializers.ModelSerializer):
    homeworld = TrackListingField(read_only=True)
    people = TrackListingField(many=True, read_only=True)

    class Meta:
        model = Species
        exclude = ('created', 'edited')
        # depth = 1


class FilmSerializer(serializers.ModelSerializer):
    characters = TrackListingField(many=True, read_only=True)
    planets = TrackListingField(many=True, read_only=True)
    starships = TrackListingField(many=True, read_only=True)
    vehicles = TrackListingField(many=True, read_only=True)
    species = TrackListingField(many=True, read_only=True)

    class Meta:
        model = Film
        exclude = ('created', 'edited')
        # depth = 1
