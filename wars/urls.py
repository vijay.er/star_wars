from django.urls import path
from wars import views

urlpatterns = [
    path('planet/<int:pk>', views.planet, name='planet'),
    path('people/<int:pk>', views.people, name='people'),
    path('film/<int:pk>', views.film, name='film'),
    path('starship/<int:pk>', views.starship, name='starship'),
    path('vehicle/<int:pk>', views.vehicle, name='vehicle'),
    path('species/<int:pk>', views.species, name='species'),


    path('people_details/', views.people_details),
    path('planet_details/', views.planet_details),
    path('film_details/', views.film_details),
    path('starship_details/', views.starship_details),
    path('vehicle_details/', views.vehicle_details),
    path('species_details/', views.species_details),

]
