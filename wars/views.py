from rest_framework import status, generics
from rest_framework.response import Response
from rest_framework.decorators import api_view
from .serializers import *
from .pagination import CustomPagination


@api_view(['GET', 'PUT', 'DELETE'])
def planet(request, pk):
    """
        get     : Getting particular planet details
        put     : update the particular planet details
        delete  : delete the particular planet details
    """
    try:
        api = Planet.objects.get(id=pk)
    except:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = PlanetSerializer(api)
        return Response(serializer.data)

    elif request.method == 'PUT':
        serializer = PlanetSerializer(api, data=request.data)

        if serializer.is_valid():
            obj = serializer.save()

            return Response([{"info": {
                "status": "SUCCESS",
                "message": "planet Details Successfully updated"
            },
                "planet_id": obj.pk,
                "planet_name": obj.name
            }], status=status.HTTP_201_CREATED)

        return Response({
                "message": "planet deleted successfully"
            }, serializer.errors)

    elif request.method == 'DELETE':
        api.delete()

        return Response(status=status.HTTP_404_NOT_FOUND)


@api_view(['GET', 'PUT', 'DELETE'])
def people(request, pk):
    """
        get     : Getting particular people details
        put     : update the particular people details
        delete  : delete the particular people details
    """
    try:
        api = People.objects.get(id=pk)
    except:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = PeopleSerializer(api)
        return Response(serializer.data)

    elif request.method == 'PUT':
        serializer = PeopleSerializer(api, data=request.data)

        if serializer.is_valid():
            obj = serializer.save()

            return Response([{"info": {
                "status": "SUCCESS",
                "message": "people Details Successfully updated"
            },
                "people_id": obj.pk,
                "people_name": obj.name
            }], status=status.HTTP_201_CREATED)

        return Response(serializer.errors)

    elif request.method == 'DELETE':
        api.delete()

        return Response({
                "message": "people deleted successfully"
            }, status=status.HTTP_404_NOT_FOUND)


@api_view(['GET', 'PUT', 'DELETE'])
def film(request, pk):
    """
        get     : Getting particular film details
        put     : update the particular film details
        delete  : delete the particular film details
    """
    try:
        api = Film.objects.get(id=pk)

    except:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = FilmSerializer(api)

        return Response(serializer.data)

    elif request.method == 'PUT':
        serializer = FilmSerializer(api, data=request.data)

        if serializer.is_valid():
            obj = serializer.save()

            return Response([{"info": {
                "status": "SUCCESS",
                "message": "film Details Successfully Updated"
            },
                "film_id": obj.pk,
                "film_title": obj.title
            }], status=status.HTTP_201_CREATED)

        return Response(serializer.errors)

    elif request.method == 'DELETE':
        api.delete()

        return Response({
                "message": "film deleted successfully"
            }, status=status.HTTP_404_NOT_FOUND)


@api_view(['GET', 'PUT', 'DELETE'])
def starship(request, pk):
    """
        get     : Getting particular starship details
        put     : update the particular starship details
        delete  : delete the particular starship details
    """
    try:
        api = Starship.objects.get(id=pk)

    except:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = StarshipSerializer(api)

        return Response(serializer.data)

    elif request.method == 'PUT':
        serializer = StarshipSerializer(api, data=request.data)
        if serializer.is_valid():
            obj = serializer.save()
            return Response([{"info": {
                "status": "SUCCESS",
                "message": "starship Details Successfully updated"
            },
                "starship_id": obj.pk,
                "starship_name": obj.name
            }], status=status.HTTP_201_CREATED)
        return Response(serializer.errors)

    elif request.method == 'DELETE':
        api.delete()

        return Response({
                "message": "starship deleted successfully"
            }, status=status.HTTP_404_NOT_FOUND)


@api_view(['GET', 'PUT', 'DELETE'])
def vehicle(request, pk):
    """
        get     : Getting particular vehicle details
        put     : update the particular vehicle details
        delete  : delete the particular vehicle details
    """
    try:
        api = Vehicle.objects.get(id=pk)

    except:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = VehicleSerializer(api)

        return Response(serializer.data)

    elif request.method == 'PUT':

        serializer = VehicleSerializer(api, data=request.data)
        if serializer.is_valid():
            obj = serializer.save()
            return Response([{"info": {
                "status": "SUCCESS",
                "message": "vehicle Details Successfully updated"
            },
                "vehicle_id": obj.pk,
                "vehicle_name": obj.name
            }], status=status.HTTP_201_CREATED)

        return Response(serializer.errors)

    elif request.method == 'DELETE':
        api.delete()

        return Response({
                "message": "vehicle deleted successfully"
            }, status=status.HTTP_404_NOT_FOUND)


@api_view(['GET', 'PUT', 'DELETE'])
def species(request, pk):
    """
        get     : Getting particular species details
        put     : update the particular species details
        delete  : delete the particular species details
    """
    try:
        api = Species.objects.get(id=pk)
    except:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = SpeciesSerializer(api)
        return Response(serializer.data)

    elif request.method == 'PUT':
        serializer = SpeciesSerializer(api, data=request.data)

        if serializer.is_valid():
            obj = serializer.save()

            return Response([{"info": {
                "status": "SUCCESS",
                "message": "species Details Successfully updated"
            },
                "species_id": obj.pk,
                "species_name": obj.name
            }], status=status.HTTP_201_CREATED)

        return Response(serializer.errors)

    elif request.method == 'DELETE':
        api.delete()

        return Response({
                "message": "species deleted successfully"
            }, status=status.HTTP_404_NOT_FOUND)


@api_view(['GET', 'POST'])
def people_details(request):
    """
        get     : Getting list of people details
        post    : upload the  people details
    """
    if request.method == 'GET':
        queryset = People.objects.all()
        paginator = CustomPagination()
        result = paginator.paginate_queryset(queryset, request)
        serializer = PeopleSerializer(result, many=True)
        return paginator.get_paginated_response(serializer.data)

    if request.method == 'POST':
        serializer = PeopleSerializer(data=request.data)

        if serializer.is_valid():
            obj = serializer.save()

            return Response([{"info": {
                "status": "SUCCESS",
                "message": "people Details Successfully Uploaded"
            },
                "people_id": obj.pk,
                "people_name": obj.name
            }], status=status.HTTP_201_CREATED)

        return Response(serializer.errors)


@api_view(['GET', 'POST'])
def planet_details(request):
    """
        get     : Getting list of planet details
        post    : upload the  planet details
    """

    if request.method == 'GET':
        queryset = Planet.objects.all()
        paginator = CustomPagination()
        result = paginator.paginate_queryset(queryset, request)
        serializer = PlanetSerializer(result, many=True)
        return paginator.get_paginated_response(serializer.data)

    if request.method == 'POST':
        serializer = PlanetSerializer(data=request.data)

        if serializer.is_valid():
            obj = serializer.save()

            return Response([{"info": {
                "status": "SUCCESS",
                "message": "planet Details Successfully Uploaded"
            },
                "planet_id": obj.pk,
                "planet_name": obj.name
            }], status=status.HTTP_201_CREATED)

        return Response(serializer.errors)


@api_view(['GET', 'POST'])
def film_details(request):
    """
        get     : Getting list of film details
        post    : upload the  film details
    """

    if request.method == 'GET':
        queryset = Film.objects.all()
        paginator = CustomPagination()
        result = paginator.paginate_queryset(queryset, request)
        serializer = FilmSerializer(result, many=True)
        return paginator.get_paginated_response(serializer.data)

    if request.method == 'POST':
        serializer = FilmSerializer(data=request.data)

        if serializer.is_valid():
            obj = serializer.save()

            return Response([{"info": {
                "status": "SUCCESS",
                "message": "film Details Successfully Uploaded"
            },
                "film_id": obj.pk,
                "film_name": obj.title
            }], status=status.HTTP_201_CREATED)

        return Response(serializer.errors)


@api_view(['GET', 'POST'])
def starship_details(request):
    """
        get     : Getting list of starship details
        post    : upload the  starship details
    """
    if request.method == 'GET':
        queryset = Starship.objects.all()
        paginator = CustomPagination()
        result = paginator.paginate_queryset(queryset, request)
        serializer = StarshipSerializer(result, many=True)
        return paginator.get_paginated_response(serializer.data)

    if request.method == 'POST':
        serializer = StarshipSerializer(data=request.data)

        if serializer.is_valid():
            obj = serializer.save()

            return Response([{"info": {
                "status": "SUCCESS",
                "message": "starship Details Successfully Uploaded"
            },
                "starship_id": obj.pk,
                "starship_name": obj.name
            }], status=status.HTTP_201_CREATED)

        return Response(serializer.errors)


@api_view(['GET', 'POST'])
def vehicle_details(request):
    """
        get     : Getting list of vehicle details
        post    : upload the  vehicle details
    """
    if request.method == 'GET':
        data = Vehicle.objects.all()
        paginator = CustomPagination()
        result = paginator.paginate_queryset(data, request)
        serializer = VehicleSerializer(result, many=True)
        return paginator.get_paginated_response(serializer.data)

    if request.method == 'POST':
        serializer = VehicleSerializer(data=request.data)

        if serializer.is_valid():
            obj = serializer.save()

            return Response([{"info": {
                "status": "SUCCESS",
                "message": "vehicle Details Successfully Uploaded"
            },
                "vehicle_id": obj.pk,
                "vehicle_name": obj.name
            }], status=status.HTTP_201_CREATED)

        return Response(serializer.errors)


@api_view(['GET', 'POST'])
def species_details(request):
    """
        get     : Getting list of species details
        post    : upload the  planet details
    """
    if request.method == 'GET':
        data = Species.objects.all()
        paginator = CustomPagination()
        result_page = paginator.paginate_queryset(data, request)
        serializer = SpeciesSerializer(result_page, many=True)

        return paginator.get_paginated_response(serializer.data)

    if request.method == 'POST':
        serializer = SpeciesSerializer(data=request.data)

        if serializer.is_valid():
            obj = serializer.save()

            return Response([{"info": {
                "status": "SUCCESS",
                "message": "species Details Successfully Uploaded"
            },
                "species_id": obj.pk,
                "species_name": obj.name
            }], status=status.HTTP_201_CREATED)

        return Response(serializer.errors)

# class SpeciesList(generics.ListAPIView):
#     queryset = Species.objects.all()
#     serializer_class = SpeciesSerializer