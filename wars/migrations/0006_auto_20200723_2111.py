# Generated by Django 3.0.8 on 2020-07-23 15:41

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('wars', '0005_auto_20200723_2010'),
    ]

    operations = [
        migrations.AlterField(
            model_name='hero',
            name='homeworld',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='heroes', to='wars.Planet'),
        ),
    ]
